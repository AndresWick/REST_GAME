package com.cliente;
import com.persistencia.Usuario;
//import com.sun.jersey.api.client.ClientResponse;
import javax.ws.rs.core.GenericType;
import javax.ws.rs.core.Response;
//import com.sun.jersey.api.client.GenericType;
import java.util.ArrayList;
import java.util.List;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;    

@ManagedBean()
@SessionScoped
public class MetodosUsuario {
    
    public List<Usuario> buscarUsuarios(){
        NewJerseyClientUser client1=new NewJerseyClientUser();
        Response response= client1.findAll_XML(Response.class);
 
        GenericType<List<Usuario>> genericType = new GenericType<List<Usuario>>() {};
        List<Usuario> data= new ArrayList<Usuario>();
        data=(response.readEntity(genericType));
        
        return data;
 }
 
    public void agregarUsuario(String nombre){
        NewJerseyClientUser client1=new NewJerseyClientUser();
        Usuario user = new Usuario();
        user.setNombreusuario(nombre);
        client1.create_XML(user);
    }
    
    public void buscarUsuario(){
    
    }
     
}