/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cliente;

import com.persistencia.Partida;
import com.persistencia.Usuario;
import java.util.ArrayList;
import java.util.List;
import javax.ws.rs.core.GenericType;
import javax.ws.rs.core.Response;

/**
 *
 * @author Carlos Aguirre.
 */
public class MetodosPartida {
    
    public void agregarPartida(Usuario usuario,String dif){
        NewJerseyClient client1=new NewJerseyClient();
        Partida partida = new Partida();
        partida.setIdusuario(usuario);
        partida.setPuntaje(0);
        partida.setDificultad(dif);
        client1.create_XML(partida);
    }
    
    public List<Partida> buscarPartidas(){
        NewJerseyClient client=new NewJerseyClient();
        Response response = client.findAll_XML(Response.class);
 
        GenericType<List<Partida>> genericType = new GenericType<List<Partida>>() {};
        List<Partida> data= new ArrayList<Partida>();
        data=(response.readEntity(genericType));
        
        return data;
    }
    
    public void editPuntaje(Partida partida){
        NewJerseyClient client=new NewJerseyClient();
        client.edit_XML(partida,""+partida.getIdpartida());
    }
    
}
